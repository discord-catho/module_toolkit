# Release Notes

## Version 1.1.2b2
15/10/2023

### New Features
- Cron_to_Schedule

### Improvements
- Cron_to_Schedule available for coroutines (Discord.py functions)

## Version 1.1.1b2
27/09/2023

### New Features
- rsa_key

### Improvements


## Version 1.1.0
20/09/2023

### New Features
- dict.unprefix_keys
- discordify, discordify_dict
- str.split_content added (from discord_breviare)
- add async envvar.task_watch_dotenv
- add verbose.write_message to switch between logging and writes

### Improvements
- Use Markdownify to convert HTML to Markdown
- parserhtml: End corrections for the new HTML parser
- parserhtml.clean_html : add argument "replace_tags"
- compatMod : rewrite function, change name (compat_mode), add usage in doc
- html.request_html_page : rewrite, add several try capability, add format_ouput (html/json)
- move str.py to str submodule
- envvar as submodule
- documentation
- version of requirements for CI

## Version 1.0.3b
14/07/2023

### New Features
- jsonlike : clean_json, replace_empty_strings_with_none
- dict : invert_dict

### Bug fixes
- recurs_function : add security : function is only a function


## Version 1.0.2
21/06/2023

### New Features
- datestr.is_valid_date added
- envvar.assign_envvar
- html.request_html_page
- html.read_html_file
- list.aplatir_liste
- ParserHTML added from dtkoparser
- parserHTML.getdata
# - sqlite3.recursive_sql

### Bug Fixes
- sqlite3.py : add call to os library
- ParserHTML : add self.equivalents_html to get equivalents of characters (exemple : &minus; and not &#8722;)
- ParserHTML._removeDuplicates : rewrite (was not working before)
- ParserHTML.simplify_html : convert special characters to HTML with UnicodeDammit.unicode_markup
- ParserHTML.utf8_to_html : convert special characters to HTML with UnicodeDammit.unicode_markup before use dict + self.equivalents_html
- Package creation
- Line profiler for tests + html report

## Version 1.0.1
05/06/2023

### New Features
- Verbose mode (todo : for all)
- Unit test
- Continous integration : pep8 + covering

### Bug Fixes
- Continous Integration : not create tag if exists
- Requirements : add category integration

### Improvements
- Move ./src/dktotoolkit to ./dktotoolkit
- README : add tag names directive
- Add LICENCE
- Add RELEASE_NOTES
- Improve setup.py, setup.cfg : use now pkg/__version__.py, requirements.txt for changements
- Add setup.rst
- Add release_notes.rst
- Follow PEP8 directives
- Add badges

## Version 1.0.0
02/06/2023

### New Features
- loadend is now in this project


### Improvements
- Continous Integration, with auto push in Pypi, but not for alpha and release candidates