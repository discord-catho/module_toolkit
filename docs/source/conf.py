import os
import sys
#sys.path.insert(0, os.path.abspath('../..'))

import dktotoolkit
#os.environ["PYTHONPATH"] = "../"


# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = dktotoolkit.__title__
copyright = dktotoolkit.__copyright__
author = dktotoolkit.__author__
release = dktotoolkit.__version__

version_path = os.path.abspath(os.path.join("../..", dktotoolkit.__pkg_name__, "__version__.py"))
if not os.path.exists(version_path):
    raise ValueError(f"{version_path} not exists")
#endIf

rst_prolog = f"""
.. |version_file| replace:: {version_path}
.. |pkg_name| replace:: {dktotoolkit.__pkg_name__}
"""


rst_prolog = f"""
.. |version_file| replace:: {version_path}
"""

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'sphinx.ext.autodoc',  # Autodoc
    'sphinx.ext.autosummary',  # Ne pas ecrire les autoX
    'sphinx.ext.viewcode', # Arborescence
    "myst_parser",    # Add the README
]

# inutile :
#'sphinx_substitution_extensions'
# A ajouter :
#    "sphinx.ext.intersphinx",
#    "sphinx.ext.todo",


templates_path = ['_templates']
exclude_patterns = []

language = 'fr'

os.environ['SPHINX_BUILD'] = '1'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
# html_static_path = ['_static']

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',
    'special-members': '__init__',
    'undoc-members': True,
    'exclude-members': '__weakref__',
    'no-module-first':True
}
