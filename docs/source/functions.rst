Dates
=====

.. automodule::
   dktotoolkit.datestr


Dictionnary
===========
.. automodule::
   dktotoolkit.dict

List
====
.. automodule::
   dktotoolkit.list

Str
===
.. automodule::
   dktotoolkit.str


Functions
=========
.. automodule::
   dktotoolkit.functions

.. automodule::
   dktotoolkit.function_recursive

HTML
====
.. automodule::
   dktotoolkit.html

SQL
===
.. automodule::
   dktotoolkit.sqlite3

Json
====

.. automodule::
   dktotoolkit.jsonlike




