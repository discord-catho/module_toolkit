.. ParserHTML documentation master file, created by
   sphinx-quickstart on Tue May 23 12:49:23 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation du dktoToolkit
============================

Ce module sert de boite à outil de base pour d'autres modules.
Le but étant qu'il soit le plus solide possible, des tests unitaires sont mis en place dessus.


Le sous-module ParserHTML est utilisé pour convertir des fichiers :

* utf8-html -> ascii-html
* ascii-html -> utf8-html
* ascii-html -> utf8-markdown

En un mot, pour convertir des données venant d'API vers du Markdown interprétable par Discord ;)


.. image:: https://framagit.org/discord-catho/module_toolkit/badges/main/pipeline.svg)]
   :alt: Pipeline status
   :target: https://framagit.org/discord-catho/module_toolkit/-/commits/main
.. image:: https://framagit.org/discord-catho/module_toolkit/badges/main/release.svg)]
   :alt: Latest Release
   :target: https://framagit.org/discord-catho/module_toolkit/-/releases

.. image:: https://img.shields.io/endpoint?url=https://discord-catho.frama.io/module_toolkit/badges/pep8-rate.json
   :alt: PEP8 - Rate of errors (lines with errors / total lines)
   :target: https://discord-catho.frama.io/module_toolkit/flake8_report/
.. image:: https://img.shields.io/endpoint?url=https://discord-catho.frama.io/module_toolkit/badges/pep8-critical.json
   :alt: PEP8 - Critical errors
   :target: https://discord-catho.frama.io/module_toolkit/flake8_report/
.. image:: https://img.shields.io/endpoint?url=https://discord-catho.frama.io/module_toolkit/badges/pep8-noncritical.json
   :alt: PEP8 - Warnings
   :target: https://discord-catho.frama.io/module_toolkit/flake8_report/
.. image:: https://img.shields.io/static/v1?label=Profiling&message=yep&color=informational
   :alt: PEP8 - Warnings
   :target: https://discord-catho.frama.io/module_toolkit/profiler_report

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   setup
   release_notes

   parserhtml
   loadenv
   logging
   discordify
   functions

   reports


* git : https://framagit.org/discord-catho/module_toolkit
* Pypi : https://pypi.org/project/dktotoolkit

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
