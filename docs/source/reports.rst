Rapports
========

PEP8
----

Outil : flake8

`<flake8_report>`_


Couverture
----------

Outil :  coverage.py

`<coveragepy_report>`_


Profiler
========

Outils : lin_profiler, html_report_line_profiler

`<profiler_report>`_
