Configuration of package
========================

Setup
-----

.. literalinclude:: ../../setup.cfg
   :language: ini
   :caption: Contents of the setup.cfg file
   :linenos:

Version
-------

.. literalinclude:: ../../dktotoolkit/__version__.py
   :language: python3
   :caption: Contents of the __version__.py file
   :linenos:


Requirements
------------
.. literalinclude:: ../../requirements.txt
   :language: ini
   :caption: Contents of the requirements.txt file
   :linenos:
