import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

def check_private_key(
        public_key=None,
        public_key_path=None,
        environ_public_key="RSA_PUBLIC_KEY",
        environ_public_key_path="RSA_PUBLIC_KEY_PATH",
        private_key=None,
        private_key_path=None,
        environ_private_key="RSA_PRIVATE_KEY",
        environ_private_key_path="RSA_PRIVATE_KEY_PATH",
        private_passwd=None,
        environ_private_passwd="RSA_PRIVATE_KEY_PASSWD",
):

    """
    Vérifie si une clé privée correspond à une clé publique donnée.
    D'une façon générale, le code a accès à la clé privée, et l'utilisateur entre la clé publique.

    :param str private_key: Clé privée au format PEM
    :param str,optional public_key: Clé publique au format PEM (facultatif)
    :param str,optional public_key_path: Chemin vers un fichier contenant la clé publique (facultatif)
    :param str,optional environ_public_key: Nom de la variable d'environnement contenant la clé publique (par défaut : "RSA_PUBLIC_KEY")
    :param str,optional environ_public_key_path: Nom de la variable d'environnement contenant le chemin vers le fichier de la clé publique (par défaut : "RSA_PUBLIC_KEY_PATH")
    :param str,optional environ_private_passwd: Nom de la variable d'environnement contenant le mot de passe de la clé privée (par défaut : "RSA_PRIVATE_KEY_PASSWD")
    :return: True si la clé privée correspond à la clé publique, False sinon
    :rtype: bool

    :raises: ValueError si les clés ne correspondent pas ou s'il y a une erreur lors du chargement
    """

    def loadkey(key, key_path, environ_key, environ_key_path):

        if key is None and not os.environ.get(environ_key):
            if key_path is None:
                key_path = os.environ.get(environ_key_path)
            #

            if key_path is None:
                raise ValueError("The path for the key is still None here")
            #

            # Charger la clé publique depuis le fichier
            with open(key_path, 'rb') as key_file:
                key_bytes = key_file.read()
            #
        else:
            if key is None:
                key = os.environ.get(environ_key)
            #

            key_bytes = key.encode('utf-8')  # Convertit la clé publique en bytes
        #
        return key_bytes
    #

    # Lire la cle publique
    public_key_bytes = loadkey(
        key=public_key,
        key_path=public_key_path,
        environ_key=environ_public_key,
        environ_key_path=environ_public_key_path
    )

    # Charger la cle publique
    try:
        cle_publique = serialization.load_pem_public_key(
            public_key_bytes,
            backend=default_backend()
        )
    except Exception as e:
        print(e)
        raise
    #

    # Lire la cle privee
    private_key_bytes = loadkey(
        key=private_key,
        key_path=private_key_path,
        environ_key=environ_private_key,
        environ_key_path=environ_private_key_path
    )

    # Lire le mot de passe
    if private_passwd is None:
        private_passwd = os.environ.get(environ_private_passwd, None)
    #
    if private_passwd is None:
        passwd_bytes = None
    else:
        passwd_bytes = private_passwd.encode('utf-8')  # Convertit la clé privée en bytes
    #

    # Charger la cle privee
    cle_privee = serialization.load_pem_private_key(
        private_key_bytes,
        password=passwd_bytes,
        backend=default_backend()
    )
    # Récupère la clé publique correspondant à la clé privée
    cle_publique_privee = cle_privee.public_key()

    # Vérifie si les clés correspondent
    return cle_publique == cle_publique_privee
#
