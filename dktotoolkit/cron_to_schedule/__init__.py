import schedule
import time
import functools
from typing import Callable
import asyncio
import datetime
import inspect
import logging

try:
    from ..verbose import write_message
except ImportError:
    def write_message():
        pass
    #
#


try:
    from _prepare_schedule_params import prepare_schedule_params
    from __constants import *
except ImportError:
    from ._prepare_schedule_params import prepare_schedule_params
    from .__constants import *
#


def _todict(obj):
    return dict((k, getattr(obj, k)) for k in dir(obj) if not k.startswith('_'))


logger = logging.getLogger("cron_to_schedule")

class myJob(schedule.Job):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    #

    def run(self):
        """
        Run the job and immediately reschedule it.
        If the job's deadline is reached (configured using .until()), the job is not
        run and CancelJob is returned immediately. If the next scheduled run exceeds
        the job's deadline, CancelJob is returned after the execution. In this latter
        case CancelJob takes priority over any other returned value.

        :return: The return value returned by the `job_func`, or CancelJob if the job's
                 deadline is reached.

        """
        #return super().run()

        if self._is_overdue(datetime.datetime.now()):
            logger.debug("Cancelling job %s", self)
            return CancelJob

        logger.debug("Running job %s", self)

        func = self.job_func.func
        args = self.job_func.args
        kwargs = self.job_func.keywords
        if inspect.iscoroutinefunction(func):
            ret = asyncio.create_task(func(*args, **kwargs))
        else:
            ret = func(*args, **kwargs)
        #
        self.last_run = datetime.datetime.now()
        self._schedule_next_run()

        if self._is_overdue(self.next_run):
            logger.debug("Cancelling job %s", self)
            return CancelJob
        #
        return ret
    #
#

class Cron_to_Schedule(object):

    '''
    L'idee est que ce soit dans un style cron :
    gérer, exceptionnellement, les dates fixes, mais pas de liste de dates fixes.
    '''
    class DefaultOptions():
        years = None
        months = None
        days = []
        hours = []
        minutes = []
        seconds = None   # Not implemented

        # cron string
        cron_string = None

        # datetime lib :
        datetime = None  # Nouvelle option datetime
        time = None      # Nouvelle option time
        date = None      # Nouvelle option date

        #
        default_hour = 0
        default_minute = 0
        default_second = 0
        default_interval = 1

        tag_category=None
        tag=None

        pass
    #

    class Options():
        pass
    #

    def __init__(self, scheduler=None, job=None, job_args=[], job_kwargs={}, thread=None, next_job_id=1, **options):
        '''
        Create a schedule task from datas from cron
        '''
        # schedule.every().hour.do(greet, 'Monica').tag(tag_name, tag_category)

        # Pre-init
        if scheduler:
            self.scheduler = scheduler
        else:
            self.scheduler = schedule.Scheduler()
        #

        self.thread = thread

        self.job = functools.partial(job, *job_args, **job_kwargs)

        if not self.job and not instance.isfunction(self.job): # + tester typing.Callable
            print("not job !!!!")
        #

        self.options = _todict(self.DefaultOptions)
        self.options.update(_todict(self.Options))
        self.options.update(options)
        #

        self.jobs = {}
        self.next_job_id = next_job_id

        #
        is_time_tables = self.options["days"] or self.options["hours"] or self.options["minutes"] or self.options["seconds"]
        is_date_tables = self.options["years"] or self.options["months"] or self.options["days"]

        if self.options["datetime"] and is_time_tables:
            raise ValueError("Can not have (hour, minute, second) and datetime.datetime")
        elif self.options["time"] and is_time_tables:
            raise ValueError("Can not have (hour, minute, second) and datetime.time")
        elif self.options["date"] and is_time_tables:
            raise ValueError("Can not have (hour, minute, second) and datetime.date")
        elif self.options["date"] and is_date_tables:
            raise ValueError("Can not have (year, month, day) and datetime.date")
        elif self.options["cron_string"] and is_time_tables:
            raise ValueError("Can not have (year, month, day) and cronstring")
        elif self.options["cron_string"] and is_date_tables:
            raise ValueError("Can not have (hour, minute, second) and cronstring")
        elif self.options["cron_string"] and self.options["datetime"]:
            raise ValueError("Can not have cron_string and datetime.datetime")
        elif self.options["cron_string"] and self.options["time"]:
            raise ValueError("Can not have cron_string and datetime.time")
        elif self.options["cron_string"] and self.options["date"]:
            raise ValueError("Can not have cron_string and datetime.date")
        #

        # Handle cron string
        self._prepare_schedule_cronstring(self.options["cron_string"])

        # Handle new options datetime, time, and date
        self._prepare_schedule_datetime(self.options["datetime"])
        self._prepare_schedule_time(self.options["time"])
        self._prepare_schedule_date(self.options["date"])
        #

        # init
        self.list_tasks = prepare_schedule_params(
            days=self.options["days"],
            hours=self.options["hours"],
            minutes=self.options["minutes"]
        )
        # [{'period': 'hour', 'every': 1, 'hour': None, 'minute': '20'}, ]

        for i in range(len(self.list_tasks)):
            if set(self.list_tasks[i].keys()) != set([HOUR, MINUTE, PERIOD, INTERVAL]):
                msg = (f"error on {self.list_tasks[i]} : "
                       "expected keys {[HOUR, MINUTE, PERIOD, INTERVAL]}"
                       )
                raise ValueError(msg)
            #
            hour = self.list_tasks[i][HOUR]
            minute = self.list_tasks[i][MINUTE]

            time_clock = ""
            if hour is not None and hour > 0:
                time_clock = f"{hour:02}:"
            #
            if minute is not None and time_clock:
                time_clock = f"{time_clock}:{minute:02}:{self.options['default_second']:02}"
            elif minute is not None:
                time_clock = f"{minute:02}:{self.options['default_second']:02}"
            elif time_clock:
                time_clock = f"{time_clock}:{self.options['default_minute']:02}:{self.options['default_second']:02}"
            #else:
            #    time_clock = f"{self.options['default_hour']:02}:{self.options['default_minute']:02}:{self.options['default_second']:02}"
            #
            if not time_clock:
                time_clock =  f":{self.options['default_second']:02}"

            self.list_tasks[i]["clock"] = time_clock
            del self.list_tasks[i][HOUR]
            del self.list_tasks[i][MINUTE]
        #
    #


    def add_job(self):

        job_ids = []
        jobs = []

        for each_task in self.list_tasks:

            job_ids.append(self.next_job_id)
            self.next_job_id += 1

            period = each_task.get(PERIOD, None)
            interval = each_task.get(INTERVAL, self.options["default_interval"])
            clock = each_task["clock"]

            try:
                job = self.create_one_job(period=period, interval=interval, clock=clock)
            except:
                print("1>", period, interval, clock)
                print("2>", each_task)
                raise
            #

            self.jobs[job_ids[-1]] = job

            if self.thread and self.job:
                # job = job.do(self.thread, functools.partial(run_func, self.job))
                job = job.do(self.thread, self.job)
            elif self.job:
                # job = job.do(functools.partial(run_func, self.job))
                job = job.do(self.job)
            else:
                write_message("No job defined !", level="warning")
                raise ValueError("No job defined !")
            #

            tags = []
            if self.options["tag"]:
                tags.append(self.options["tag"])
            #
            if self.options["tag_category"]:
                tags.append(self.options["tag_category"])
            #

            if tags:
                job = job.tag(*tags)
            #
            jobs.append(job)
            # Do not do self.scheduler.jobs.append(job) :
            # jobs already linked with the constructor Job !
        #


        return jobs, job_ids
    #

    def create_one_job(self, period, interval, clock):
        job = myJob(interval=interval, scheduler=self.scheduler)

        if period is None:
            raise ValueError(f"Unexpected here")
        elif period in [DAY[0], HOUR, MINUTE]:
            job.unit=period
        elif period in DAY[1:8]:
            if job.interval != 1:
                raise IntervalError("Only interval==1 implemented in schedule !")
            #
            job.start_day=period
        #

        if clock:
            if period == MINUTE:
                clock = ":"+clock.split(":")[-1]
            #
            job = job.at(clock)
        #

        return job
    #

    def remove_job(self, job_id):
        if job_id in self.jobs:
            self.jobs[job_id].cancel()
            del self.jobs[job_id]

    def _prepare_schedule_cronstring(self, cronstring):
        """
        # To define the time you can provide concrete values for
        # minute (m), hour (h), day of month (dom), month (mon),
        # and day of week (dow) or use '*' in these fields (for 'any').
        Format : * * * * *
        """
        if cronstring is None:
            return
        #

        l = [e.strip() for e in cronstring.split()]
        self.options["minutes"], self.options["hours"], _, self.options["months"], self.options["days"] = l
        self.options["cron_string"] = None
        #

    def _prepare_schedule_datetime(self, datetime_obj):
        if datetime_obj is None:
            return
        #
        self.options["year"] = [datetime_obj.year,]
        self.options["month"] = [datetime_obj.month,]
        self.options["day"] = [datetime_obj.year,]
        self.options["hours"] = [datetime_obj.hour,]
        self.options["minutes"] = [datetime_obj.minute,]
        self.options["seconds"] = [datetime_obj.seconds,]
        raise ValueError("Not implemented yet datetime.datetime")
    #

    def _prepare_schedule_time(self, time_obj):
        if time_obj is None:
            return
        #
        self.options["hours"] = [time_obj.hour,]
        self.options["minutes"] = [time_obj.minute,]
        self.options["seconds"] = [time_obj.seconds,]
    #

    def _prepare_schedule_date(self, date_obj):
        if date_obj is None:
            return
        #
        self.options["year"] = [datetime_obj.year,]
        self.options["month"] = [datetime_obj.month,]
        self.options["day"] = [datetime_obj.year,]
        raise ValueError("Not implemented yet datetime.date")
    #
#

def cron_to_schedule(job, days:list=[], hours:list=[], minutes:list=[],job_args:list=[], job_kwargs:dict={}, **options):
    '''
    Transformer en schedule
    '''

    return Cron_to_Schedule(job=job, days=days, hours=hours, minutes=minutes, job_args=job_args, job_kwargs=job_kwargs,**options).add_job()
#


if __name__=="__main__":
    import time
    def coucou():
        print("coucou")
    #
    def hello():
        print("hello")
    #
    scheduler = schedule.Scheduler()
    cron_to_schedule(job=coucou, tag="hello", minutes=["/2","/3"], scheduler=scheduler)
    #cron_to_schedule(job=hello, tag="hello1", minutes=[51,], scheduler=scheduler)

    print("JOBS", scheduler.get_jobs())
    while True:
        scheduler.run_pending()
        time.sleep(1)
    #

#

"""
a lire :
https://schedule.readthedocs.io/en/stable/exception-handling.html
https://schedule.readthedocs.io/en/stable/timezones.html
"""
