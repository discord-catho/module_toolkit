# python3 -m unittest discover
try:
    from __constants import *
    from _update import update_days, update_hours, update_minutes
    from _tools import range_from, generate_list, parse_arg
except ImportError:
    from .__constants import *
    from ._update import update_days, update_hours, update_minutes
    from ._tools import range_from, generate_list, parse_arg
#

# ToDo : remplacer "range_from" par un bete range...

def prepare_schedule_params(days:list=[], hours:list=[], minutes:list=[], trace:bool=False):
    """
    :param str,list hours: Hours, Intervals of hours, Every certain nb of hours
    :param str,list minutes: Idem for minutes
    :param str,list days: Id of the day each week (monday=1), idem for the day

    :returns: [{"period", "every", "at_time"}, ...]
                     - period = "minute", "hour", "day", "monday", "tuesday", ...
                     - every : duration between repetitions
                     - at : string in format HH:MM for a particular hour
    :rtypes: list(dict)
    """

    result = []

    if trace:
        print("1.")
    #

    days = parse_arg(days)
    hours = parse_arg(hours)
    minutes= parse_arg(minutes)

    if trace:
        print("2.")
    #

    result_days = update_days(days=days)
    if trace:
        print("DAY >", result_days)
    #

    result_hours = update_hours(hours=hours)
    if trace:
        print("HOUR>", result_hours)
    #

    result_minutes = update_minutes(minutes=minutes)
    if trace:
        print("MIN >", result_minutes)
    #

    out = []

    if(result_days == [{PERIOD: DAY[0], INTERVAL: None}] and
       result_hours == [{PERIOD: HOUR, INTERVAL: None, HOUR: None}] and
       result_minutes == [{PERIOD: MINUTE, INTERVAL: None, MINUTE: None}] ):
        if trace:
            print("4.a")
        #
        # A ameliorer
        return None
    elif (result_hours == [{PERIOD: HOUR, INTERVAL: None, HOUR: None}] and
       result_minutes == [{PERIOD: MINUTE, INTERVAL: None, MINUTE: None}] ):
        if trace:
            print("4.b")
        #
        for r_d in result_days:
            r_d.update({HOUR:None})
        #
        return list(result_days)
    # elif (result_days == [{'period': DAY[0], 'every': None}] and
    #       result_minutes == [{'period': MINUTE, 'every': None, MINUTE: None}] ):
    #     if trace:
    #         print("4.c")
    #     #
    #     #for r_d in result_days:
    #     #    r_d.update({HOUR:None,MINUTE:None})
    #     ##
    #     return list(result_hours)
    # elif (result_days == [{'period': DAY[0], 'every': None}] and
    #       result_hours == [{'period': HOUR, 'every': None, HOUR: None}] ):
    #     if trace:
    #         print("4.d")
    #     #
    #     return list(result_minutes)
    # #

    for dico_day in result_days:

        dico_day_empty = dico_day == {PERIOD: None, INTERVAL: None}

        if trace:
            print("5.0", dico_day)
        #

        for dico_hour in result_hours:

            dico_hour_empty = dico_hour == {PERIOD: HOUR, INTERVAL: None, HOUR: None}

            if trace:
                print("5.1", dico_hour)
            #

            out_hour = []

            if dico_day_empty and  dico_hour_empty:

                # Aucune info relative au jour et a l'heure : on gerera dans les minutes
                if trace:
                    print("5.1.a0 (days and hours empty)")
                #
                out_hour.append(dico_hour.copy())

            elif dico_hour_empty:

                # Aucune information relative a l'heure : continuer avec le dico du jour
                if trace:
                    print("5.1.a2 (hours empty)")
                #
                out_hour.append(dico_day.copy())

            elif dico_day_empty:

                # Aucune information relative au jour : la base est le dico de l'heure
                if trace:
                    print("5.1.a1 (days empty)")
                #
                out_hour.append(dico_hour.copy())

            elif dico_hour[PERIOD]==HOUR and dico_hour[INTERVAL] is not None:

                # Appels recurents des heures
                # Une fréquence quotidienne est définie : on ajoute une liste d'heures

                if trace:
                    print("5.1.a3b")
                #

                L = generate_list(
                    start=0,
                    end=24,
                    interval=dico_hour[INTERVAL],
                    dico_base=dico_day.copy(),
                    key_range=HOUR
                )
                out_hour.extend(L)

            elif dico_hour[PERIOD] in DAY:

                # On a une heure définie
                # Une fréquence quotidienne est définie : on ajoute l'heure au jour
                if trace:
                    print("5.1.a4")
                #

                datas_tmp = dico_day.copy()
                datas_tmp[HOUR] = dico_hour[HOUR]
                out_hour.append(datas_tmp.copy())

            else:

                if trace:
                    print("5.1.aerr")
                #
                raise ValueError("h "+str(dico_day)+" --- "+str(dico_hour))
            #

            if  result_minutes == [{PERIOD: MINUTE, INTERVAL: None, MINUTE: None}]:
                if trace:
                    print("5.1.f")
                #
                for item in out_hour:
                    item.update({MINUTE:None})
                #
                out.extend(out_hour)
                continue
            #

            for dico_min in result_minutes:

                if trace:
                    print("5.2.a", dico_min)
                #

                out_minutes = []
                l = out_hour[-1].copy()


                if out_hour[-1] == {PERIOD: HOUR, INTERVAL: None, HOUR: None}:
                    # days et hours sont vides

                    if trace:
                        print("5.2.aa")
                    #
                    if len(out_hour) > 1:
                        raise ValueError(f"Expected length 1 but len = {len(out_hour)} : {out_hour}")
                    #

                    for e in out_hour:
                        data_tmp = e.copy()
                        data_tmp.update(dico_min)
                        out.append(data_tmp)
                    #

                elif dico_min[INTERVAL] is not None:

                    if trace:
                        print("5.2.b")
                    #
                    # Un jour est défini, pas d'heure, et un intervale de minutes
                    end = None
                    if out_hour[-1][PERIOD] in HOUR:
                        if trace:
                            print("5.2.b01")
                        #
                        end = 1
                    elif out_hour[-1][PERIOD] in DAY:
                        if trace:
                            print("5.2.b02")
                        #
                        end = 24
                    #

                    if dico_min[PERIOD] == MINUTE :
                        if trace:
                            print("5.2.b03")
                        #
                        end = end * 60-1
                    #

                    if trace:
                        print("5.2.c")
                    #

                    for e in out_hour:
                        if HOUR in e:
                            L = generate_list(
                                start=0,
                                end=end,
                                interval=dico_min[INTERVAL],
                                dico_base=e.copy(),
                                key_range=MINUTE
                            )

                            # for e in L:
                            #     if not HOUR in e.keys():
                            #         e[HOUR]=-1
                            #     #
                            # #
                        else:
                            L = [e.copy(),]
                            L[-1][MINUTE] = dico_min[MINUTE]
                            L[-1][HOUR] = -1
                        #
                        out.extend(L)
                    #

                elif out_hour[-1][PERIOD] in DAY and dico_min[INTERVAL] is None:
                    if trace:
                        print("5.2.d")
                    #
                    # Un jour est défini, pas d'heure, mais des minutes fixes
                    lh = list(range_from(start=0, end=24 , interval=1))
                    l = [dico_day.copy() for e in lh]
                    for i in range(len(l)):
                        l[i].update({HOUR:lh[i], MINUTE:dico_min[MINUTE]})
                    #
                    out.extend(l)

                else:
                    if dico_min[PERIOD] == HOUR:
                        if trace:
                            print("5.2.f1")
                        #
                        l[MINUTE] = dico_min[MINUTE]
                        out.append(l.copy())
                    elif dico_min[PERIOD] == MINUTE:
                        if trace:
                            print("5.2.f2")
                        #
                        l[MINUTE] = dico_min[MINUTE]
                        out.append(l.copy())
                #
            #
        #
    #

    result = out
    if not any([days, hours, minutes]):
        result.append(_generate_daily_schedule(f"{default_hour}:{default_minute}"))
    #

    for i in range(len(result)):
        if not result[i]:
            raise ValueError("not result"+str(result))
        #
        if not HOUR in result[i] or not MINUTE in result[i] or not PERIOD in result[i] or not INTERVAL in result[i]:
            raise ValueError("key error"+ str(result))
        #
        if result[i][HOUR] is not None:
            result[i][HOUR] = int(result[i][HOUR])
        #
        if result[i][MINUTE] is not None:
            result[i][MINUTE] = int(result[i][MINUTE])
        #
        if result[i][PERIOD] is None and result[i][INTERVAL] is None:
            result[i][PERIOD] = DAY[0]
            result[i][INTERVAL] = 1
        #
    #

    # Supprimer les doublons
    ensembles = [frozenset(d.items()) for d in result]
    result = [dict(s) for s in set(ensembles)]

    return result
#


def main2():


    print("................")
    #print(prepare_schedule_params(set(), hours=set([1, "/5", "2/10"]), minutes=set(("/30",))))

    # print(prepare_schedule_params(days=set(), hours=set(["/5",]), minutes=set()))

    # print("................")
    # print(prepare_schedule_params(days=set(), hours=set(), minutes=set([1,5,12 ])))
    # print(prepare_schedule_params(days=set(), hours=set(), minutes=set(["/5",])))
    # print(prepare_schedule_params(days=set(), hours=set(), minutes=set(["2/10",])))
    # print("................")
    # print(prepare_schedule_params(days=set([5,]), hours=set([2,4]), minutes=set([3,6,9, "20/30" ])))
    # print("................")
    print("................")
    #print(prepare_schedule_params(days=([5,]), hours=(), minutes=(["10","20" ]), trace=True))
    print(prepare_schedule_params(days=([5,]), hours=(), minutes=(["10",30 ]), trace=True))
    print("................")
#

if __name__=="__main__":
    main2()
