try:
    from __constants import INTERVAL
except ImportError:
    from .__constants import INTERVAL
#

def range_from(start, end, interval):
    new_list = []
    add = start
    i=0
    while add <= end and i < end:
        new_list.append(add)
        add += interval
        i += 1 # securite
    #
    return new_list
#

def parse_arg(arg, minarg=None, maxarg=None):

    L = []
    if arg is None:
        return []
    elif isinstance(arg, str) and "," in arg:
        L = [e.strip() for e in arg.split(",")]
    elif not isinstance(arg, (list, tuple,set)):
        L = [arg,]
    else:
        L = arg
    #

    L1 = []
    for elt in L:
        L1.extend(elt.split(","))
    #

    L2 = []
    for elt in L1:
        if "-" in elt:
            tmp = [e.strip() for e in elt.split("-")]
            if len(tmp) == 0:
                start,end = minarg, maxarg
            elif len(tmp) == 1:
                start,end = int(tmp[0]), maxarg
            elif len(tmp) == 2 and not tmp[0]:
                start,end = minarg, int(tmp[1])
            elif len(tmp) == 2:
                start,end = tmp
            else:
                raise ValueError(f"elt = {elt} not in good format X-Y with X=[int,''], Y=[int,'']")
            #

            L2.extend(list(range(start, end+1)))
        else:
            L2.append(elt)
        #
    #

    return L2
#


def generate_list(start, end, interval, dico_base, key_range, dico_update={})->list:

    lh = list(range_from(start=start, end=end, interval=interval))
    l = [dico_base.copy()  for e in lh]
    for i in range(len(l)):
        if l[i][INTERVAL] is None:
            l[i][INTERVAL] = 1
        #
        l[i].update({key_range:lh[i]})
        l[i].update(dico_update)
    #
    return l
#
