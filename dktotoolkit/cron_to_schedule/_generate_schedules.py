try:
    from __constants import *
except ImportError:
    from .__constants import *
#

def generate_minutly_schedule(interval, at_time):
    return {PERIOD: MINUTE, INTERVAL: interval, MINUTE:at_time}

def generate_hourly_schedule(interval, at_time):
    return {PERIOD: HOUR, INTERVAL: interval, HOUR: at_time}

def generate_weekly_schedule(interval, day=DAY[0]):
    return {PERIOD: day, INTERVAL: interval}
