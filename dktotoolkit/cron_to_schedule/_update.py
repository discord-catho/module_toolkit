try:
    # Si le module est exécuté en tant que module
    from __constants import *
    from _generate_schedules import *
    from _tools import range_from
except ImportError:
    # Si le module est importé comme un script principal
    from .__constants import *
    from ._generate_schedules import *
    from ._tools import range_from
#

def update_days(days=()):
    result=[]
    interval_def = 1
    for day in days:
        day = str(day)
        if "/" in day:
            if day.split("/")[0]:
                interval = int(day.split("/")[1])
                start = int(day.split("/")[0])
                end = 7
                days_tmp = range_from(start, end, interval)
                result.extend(update_days(days=days_tmp))
            else:
                interval = int(day.split("/")[1])
                result.append(generate_weekly_schedule(day=DAY[0], interval=interval))
            #
        elif "-" in day:
            raise ValueError("day-day not implemented yet !")
        elif (not day.isdigit()) and (not isinstance(day, (int, float))):
            raise ValueError(f"day error {day}")
        elif int(day) < 8 and  int(day) > 0:
            result.append(generate_weekly_schedule(day=DAY[int(day)], interval=interval_def))
        else:
            raise ValueError(f"Unexpected here ! {day}, {type(day)}")
        #
    #
    if not result:
        result.append(generate_weekly_schedule(None, None))
    #
    return result
#


def update_hours(hours=()):
    result = []
    interval_def = 1

    for hour in hours:
        hour = str(hour)
        if "/" in hour:
            if hour.split("/")[0]:
                interval = int(hour.split("/")[1])
                start = int(hour.split("/")[0])
                end = 24
                hours_tmp = range_from(start, end, interval)
                result.extend(update_hours(hours=hours_tmp))
            else:
                interval = int(hour.split("/")[1])
                result.append(generate_hourly_schedule(interval, None))
            #
        elif "-" in hour:
            raise ValueError("hour-hour not implemented yet !")
        elif not hour.isdigit() and not isinstance(hour, (int, float)):
            raise ValueError(f"hour error {hour}")
        else:
            tmp = generate_weekly_schedule(interval_def, DAY[0])
            tmp[HOUR] = hour
            result.append(tmp)
        #
    #
    if not result:
        result.append(generate_hourly_schedule(None, None))
    #
    return result


def update_minutes(minutes=[], result=[]):
    result = []
    interval_def = None

    for minute in minutes:
        minute = str(minute)
        if "/" in minute:
            if minute.split("/")[0]:
                interval = int(minute.split("/")[1])
                start = int(minute.split("/")[0])
                end = 60
                minutes_tmp = range_from(start, end, interval)
                result.extend(update_minutes(minutes=minutes_tmp))
            else:
                interval = int(minute.split("/")[1])
                result.append(generate_minutly_schedule(interval, None))
            #
        elif "-" in minute:
            raise ValueError("minute-minute not implemented yet !")
        elif not minute.isdigit() and not isinstance(minute, (int, float)):
            raise ValueError(f"minute error {hour}")
        else:
            tmp = generate_hourly_schedule(1, None)
            tmp[MINUTE] = minute
            result.append(tmp)
        #
    #
    if not result:
        result.append(generate_minutly_schedule(None, None))
    #
    return result
#
