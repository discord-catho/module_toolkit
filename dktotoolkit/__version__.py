__title__ = "toolkit"
__pkg_name__ = "dktotoolkit"
__version__ = "1.1.2b2"
__license__ = "AGPL 3"
__copyright__ = "2023, Pierre"

__git_name__ = "module_toolkit"
__git_group__ = "discord-catho"

__author__ = "Pierre"
__author_email__ = "pierre@exemple.com"

__description__ = "A little toolkit with fancy functions"
__keywords__ = "dotenv, .env, toolkit, parse dates"

__python_requires__=">=3.9, <4"
# requirements are in requirements.txt
