# -*- coding: utf-8 -*-
import os
import unittest  # python -m unittest discover
import sys
from datetime import date, timedelta

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.datestr import parser_date, date2str, is_valid_date
from dktotoolkit.exceptions import ParseError


class Test_ValidDate(unittest.TestCase):

    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(is_valid_date)
        #
    #endDef

    def test_yyyy_mm_dd(self):
        result = is_valid_date(date_str="2000-01-01")
        self.assertEqual(result, True)
    #endDef

    def test_yyyy_mm_dd_wrongsep1(self):
        result = is_valid_date(date_str="2000:01:01", date_format="yyyy-mm-dd", check_sep=True)
        self.assertEqual(result, False)
    #endDef

    def test_yyyy_mm_dd_wrongsep2(self):
        result = is_valid_date(date_str="2000:01:01", date_format="yyyy-mm-dd", check_sep=False)
        self.assertEqual(result, True)
    #endDef

    def test_yy_mm_dd(self):
        result = is_valid_date(date_str="99-01-01", date_format="yy-mm-dd")
        self.assertEqual(result, True)
    #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
#endClass


# ####################################################### #

class TestParserDate(unittest.TestCase):

    def setUp(self):
        # Code execute avant chaque test
        self.today = date.today()
        self.today_out = date2str(self.today)

        self.byesterday = date2str(date.today()+timedelta(days=-2))
        self.yesterday = date2str(date.today()+timedelta(days=-1))
        self.tomorrow  = date2str(date.today()+timedelta(days=+1))
        self.atomorrow = date2str(date.today()+timedelta(days=+2))

        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(parser_date)
        #
    #endDef

    def test_today_str(self):
        result = parser_date("today", verbose=True)  # Seul le premier cas est verbose
        self.assertEqual(result, self.today_out)
    #endDef


    def test_none(self):
        result = parser_date(None, verbose=True)  # Seul le premier cas est verbose
        self.assertEqual(result, self.today_out)
    #endDef


    def test_today_date(self):
        result = parser_date(self.today, verbose=False)
        self.assertEqual(result, self.today_out)
    #endDef


    def test_aujourdhui(self):
        result = parser_date("aujourd'hui", verbose=False)
        self.assertEqual(result, self.today_out)
    #endDef


    def test_demain(self):
        result = parser_date("demain", verbose=False)
        self.assertEqual(result, self.tomorrow)
    #endDef


    def test_tomorrow(self):
        result = parser_date("tomorrow", verbose=False)
        self.assertEqual(result, self.tomorrow)
    #endDef


    def test_apresdemain1(self):
        result = parser_date("apres-demain", verbose=False)
        self.assertEqual(result, self.atomorrow)
    #endDef


    def test_apresdemain2(self):
        result = parser_date("après-demain", verbose=False)
        self.assertEqual(result, self.atomorrow)
    #endDef

    def test_apresdemain3(self):
        result = parser_date("apres demain", verbose=False)
        self.assertEqual(result, self.atomorrow)
    #endDef

    def test_apresdemain4(self):
        result = parser_date("apres demain", verbose=False)
        self.assertEqual(result, self.atomorrow)
    #endDef

    def test_apresapresdemain(self):
        result = parser_date("apres apres demain", verbose=False)
        self.assertEqual(result, "00-00-0000")
    #endDef


    def test_hier(self):
        result = parser_date("hier", verbose=False)
        self.assertEqual(result, self.yesterday)
    #endDef


    def test_ahier(self):
        result = parser_date("avant-hier", verbose=False)
        self.assertEqual(result, self.byesterday)
    #endDef


    def test_date1(self):
        result = parser_date("2023-15-12", verbose=False)
        self.assertEqual(result, "2023-12-15")
    #endDef


    def test_date2(self):
        result = parser_date("2023-12-15", verbose=False)
        self.assertEqual(result, "2023-12-15")
    #endDef


    def test_date3(self):
        result = parser_date("15 jun 2010", verbose=True)
        self.assertEqual(result, "2010-00-15")
    #endDef


    def test_date4(self):
        result = parser_date("15 juin 2010", verbose=False)
        self.assertEqual(result, "2010-06-15")
    #endDef


    def test_date5(self):
        result = parser_date("15 june 2010", verbose=False)
        self.assertEqual(result, "2010-06-15")
    #endDef


    def test_date6(self):
        result = parser_date("15 june 10", verbose=False)
        self.assertEqual(result, "2010-06-15")
    #endDef

    def test_date7(self):
        result = parser_date("15 06 2010", verbose=False)
        self.assertEqual(result, "2010-06-15")
    #endDef

    def test_date8(self):
        result = parser_date("15:06:2010", verbose=False)
        self.assertEqual(result, "2010-06-15")
    #endDef

    def test_date9(self):
        result = parser_date("15:06:10", verbose=False)
        self.assertEqual(result, "2010-06-15")
    #endDef


    def test_date10(self):
        result = parser_date("15 06 0", verbose=False)
        self.assertEqual(result, "2000-06-15")
    #endDef


    def test_date11(self):
        result = parser_date("15 juin 0", verbose=False)
        self.assertEqual(result, "2000-06-15")
    #endDef

    def test_date_no_year1(self):
        result = parser_date("15 06", verbose=False)
        self.assertEqual(result, f"{self.today.year}-06-15")
    #endDef


    def test_date_no_year2(self):
        result = parser_date("15 juin", verbose=False)
        self.assertEqual(result, f"{self.today.year}-06-15")
    #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
#endClass
