import sys, os
import unittest

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#
sys.path.insert(0, '../')
from dktotoolkit.parserhtml import _clean_html


class TestCleanHTMLTags(unittest.TestCase):

    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_clean_html)
        #
        pass
    #endDef

    def test_clean_html_br_tags(self):
        html_content = '<p>Hello, <br>world!</p>'
        expected_output = '<p>Hello, <br/>world!</p>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_br_space_tags(self):
        html_content = '<p>Hello, <br >world!</p>'
        expected_output = '<p>Hello, <br/>world!</p>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_br_slash_tags(self):
        html_content = '<p>Hello, <br/>world!</p>'
        expected_output = '<p>Hello, <br/>world!</p>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_br_nbsp_tags(self):
        html_content = '<p>Hello, <br\xa0/>world!</p>'
        expected_output = '<p>Hello, <br/>world!</p>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_br_space_slash_tags(self):
        html_content = '<p>Hello, <br />world!</p>'
        expected_output = '<p>Hello, <br/>world!</p>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_hr_tags(self):
        html_content = '<hr class="line"  >'
        expected_output = '<hr class="line"/>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_img_tags(self):
        html_content = '<img src="image.jpg"  alt="Image" />'
        expected_output = '<img alt="Image" src="image.jpg"/>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
# endClass


class TestCleanHTMLAttributes(unittest.TestCase):

    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_clean_html)
        #
        pass
    #endDef

    def test_clean_html_class_attribute(self):
        html_content = '<p class="text  "  >Hello, world!</p>'
        expected_output = '<p class="text">Hello, world!</p>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_id_attribute(self):
        html_content = '<div id="container  ">'
        expected_output = '<div id="container"></div>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_id_attribute(self):
        html_content = '<div id="container  ">Coucou</div>'
        expected_output = '<div id="container">Coucou</div>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_href_attribute(self):
        html_content = '<a href="https://example.com"  >Link</a>'
        expected_output = '<a href="https://example.com">Link</a>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_src_attribute(self):
        html_content = '<img src="image.jpg  " alt="Image"/>'
        expected_output = '<img alt="Image" src="image.jpg"/>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_clean_html_alt_attribute(self):
        html_content = '<img src="image.jpg" alt="Image  "/>'
        expected_output = '<img alt="Image" src="image.jpg"/>'
        cleaned_html = _clean_html(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
# endClass

if __name__ == '__main__':
    unittest.main()
