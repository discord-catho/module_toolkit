import sys, os
import unittest
from bs4 import BeautifulSoup

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#
sys.path.insert(0, '../')
from dktotoolkit.parserhtml import _replace_span_with_bold_and_italic

class TestReplaceSpanWithBoldAndItalic(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_replace_span_with_bold_and_italic)
        #
        pass
    #endDef

    def test_replace_span_with_bold(self):
        html_content = '<p>Hello, <span class="chapter_number">Chapter 1</span> world!</p>'
        expected_output = '<p>Hello, <b>Chapter 1</b> world!</p>'
        replaced_html = _replace_span_with_bold_and_italic(html_content, tag_replacements = {
            'verse_number': 'i',
            'chapter_number': 'b'
        })
        self.assertEqual(replaced_html, expected_output)

    def test_replace_span_with_italic(self):
        html_content = '<p>Hello, <span class="verse_number">Verse 1</span> world!</p>'
        expected_output = '<p>Hello, <i>Verse 1</i> world!</p>'
        replaced_html = _replace_span_with_bold_and_italic(html_content, tag_replacements = {
            'verse_number': 'i',
            'chapter_number': 'b'
        })
        self.assertEqual(replaced_html, expected_output)

    def test_no_span_tags(self):
        html_content = '<p>Hello, world!</p>'
        expected_output = '<p>Hello, world!</p>'
        replaced_html = _replace_span_with_bold_and_italic(html_content)
        self.assertEqual(replaced_html, expected_output)

    def test_no_span_arguments(self):
        html_content = '<p>Hello, <span class="verse_number">Verse 1</span> world!</p>'
        expected_output = '<p>Hello, <b>Verse 1</b> world!</p>'
        replaced_html = _replace_span_with_bold_and_italic(html_content, tag_replacements = {
            'verse_number': 'b',
            'chapter_number': 'b'
        })
        self.assertEqual(replaced_html, expected_output)

    def test_multiple_span_tags(self):
        html_content = '<p><span class="chapter_number">Chapter 1</span> <span class="verse_number">Verse 1</span> world!</p>'
        expected_output = '<p><b>Chapter 1</b> <i>Verse 1</i> world!</p>'
        replaced_html = _replace_span_with_bold_and_italic(html_content, tag_replacements = {
            'verse_number': 'i',
            'chapter_number': 'b'
        })
        self.assertEqual(replaced_html, expected_output)

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

if __name__ == '__main__':
    unittest.main()
