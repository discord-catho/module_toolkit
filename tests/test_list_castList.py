# -*- coding: utf-8 -*-
import os
import unittest  # python -m unittest discover
import sys
from datetime import date, timedelta

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.list import castList


class TestList(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(castList)
        #
        pass
    #endDef


    def test_standard(self):
        result = castList("1:2:3")
        self.assertEqual(result, ['1','2','3'])
    #endDef

    def test_1(self):
        result = castList("(1,2,3),(4,5)", digit=False)
        self.assertEqual(result, [['1','2','3'],['4','5']])
    #endDef

    def test_2(self):
        result = castList("1,2,3", sep=":", digit=False)
        self.assertEqual(result, ['1','2','3'])
    #endDef

    def test_3(self):
        result = castList("(1,2,3),(4:5)", digit=False)
        self.assertEqual(result, [['1','2','3'],['4','5']])
    #endDef

    def test_4(self):
        result = castList("(1,2,3),(4,5)", digit=True)
        self.assertEqual(result, [[1,2,3],[4,5]])
    #endDef

    def test_5(self):
        result = castList("1,2,3", sep=":", digit=True)
        self.assertEqual(result, [1,2,3])
    #endDef

    def test_6(self):
        result = castList("(1,2,3),(4:5)", digit=True)
        self.assertEqual(result, [[1,2,3],[4,5]])
    #endDef

    def test_7(self):
        result = castList("(1.1,2,3),(4:5)", digit=True)
        self.assertEqual(result, [[1.1,2,3],[4,5]])
    #endDef

    def test_8(self):
        result = castList("(1.41,2,3),(4:5)", digit=False)
        self.assertEqual(result, [['1.41','2','3'],['4','5']])
    #endDef

    def test_9(self):
        result = castList("(a,2,3),(4:5),c", digit=True)
        self.assertEqual(result, [["a",2,3],[4,5],['c',]])
    #endDef

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    # endDef
# endClass
