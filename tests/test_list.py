# -*- coding: utf-8 -*-
import os
import unittest  # python -m unittest discover
import sys
from datetime import date, timedelta

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.list import replace_with_mask


class TestList(unittest.TestCase):
    def setUp(self):
        self.arr = [1,2,3,4]
        self.mask = [True, False, False, True]
        self.out = [0,2,3,0]
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(replace_with_mask)
        #
        pass
    #endDef


    def test_standard(self):
        result = replace_with_mask(self.arr, self.mask, 0, inplace=False)
        self.assertEqual(result, self.out)
    #endDef


    def test_inplace(self):
        result = replace_with_mask(self.arr, self.mask, 0, inplace=True)
        self.assertEqual(result, None)
        self.assertEqual(self.arr, self.out)
    #endDef


    # def test_not_same_length(self):
    #     result = replace_with_mask(self.arr, self.mask[0:len(self.arr) -1], 0, inplace=True)
    #     self.assertEqual(result, ValueError("Les tableaux doivent avoir la meme longueur") )
    # #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #
        pass
    #endDef
#endClass
