# -*- coding: utf-8 -*-
import unittest  # python -m unittest discover
import sys
import os
import subprocess
import io

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.str import str2digit



class Test_str2digit(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test

        with open("./test.env", "w") as f:
            f.write("TEST_VAR=1\nTEST2=3\nTEST3=4:5:6.0\n")
        #endWidth
        self.delete_profiling_env = False


        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(str2digit)
        #
    #endDef

    def test_int(self):
        result = str2digit("3")
        self.assertEqual(result, 3)
    #endDef

    def test_float(self):
        result = str2digit("3.1")
        self.assertEqual(result, 3.1)
    #endDef

    def test_list(self):
        result = str2digit("3.1:4:a", sep=":")
        self.assertEqual(result, [3.1, 4, "a"])
    #endDef


    def test_list2(self):
        result = str2digit("3.1,4,a", sep=",")
        self.assertEqual(result, [3.1, 4, "a"])
    #endDef


    def test_str(self):
        result = str2digit("a")
        self.assertEqual(result, "a")
    #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
           output_folder="./profile"
           proffile_basename = hr.names2basename(
               file_name = __file__,
               class_name = self.__class__.__name__,
               method_name = self._testMethodName
           )

           hr.tearDown_profiler(
               profiler=self.profiler,
               proffile_basename=proffile_basename,
               output_folder=output_folder,
               delete_profiling_env=self.delete_profiling_env
           )


        pass
    #endDef
#endClass
