import sys, os
import unittest
from bs4 import BeautifulSoup
import html

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#
sys.path.insert(0, '../')
from dktotoolkit.parserhtml import _decode_html_special_chars

class TestDecodeHTMLSpecialChars(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_decode_html_special_chars)
        #
        pass
    #endDef

    def test_decode_html_special_chars_utf8(self):
        html_content = '&lt;p&gt;Hello, &amp;world!&lt;/p&gt;'
        expected_output = '<p>Hello, &world!</p>'
        decoded_html = _decode_html_special_chars(html_content)
        self.assertEqual(decoded_html, expected_output)

    def test_decode_html_special_chars_latin1(self):
        html_content = '&eacute; &agrave; &ccedil;'
        expected_output = 'é à ç'
        decoded_html = _decode_html_special_chars(html_content, encoding='latin1')
        self.assertEqual(decoded_html, expected_output)


    def test_decode_html_special_chars_invalid_encoding(self):
        html_content = '&eacute; &agrave; &ccedil;'
        invalid_encoding = 'utf-888'
        with self.assertRaises(ValueError):
            _decode_html_special_chars(html_content, encoding=invalid_encoding)
        #

    def test_decode_html_special_chars_none_forcestr(self):
        html_content = None
        expected_output = ''
        decoded_html = _decode_html_special_chars(html_content, force_string=True)
        self.assertEqual(decoded_html, expected_output)

    def test_decode_html_special_chars_none_forcestr(self):
        html_content = None
        expected_output = None
        decoded_html = _decode_html_special_chars(html_content, force_string=False)
        self.assertEqual(decoded_html, expected_output)

    def test_decode_html_special_chars_empty_string(self):
        html_content = ''
        expected_output = ''
        decoded_html = _decode_html_special_chars(html_content)
        self.assertEqual(decoded_html, expected_output)

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef

if __name__ == '__main__':
    unittest.main()
