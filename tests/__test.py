from bs4 import BeautifulSoup

# Exemple de contenu HTML (remplacez cette partie par votre propre contenu HTML)
html_content = """
<!DOCTYPE html>
<html>
<head>
    <title>Mon document HTML</title>
</head>
<body>
    <div>
        <h1>Titre</h1>
        <p>Paragraphe 1</p>
        <p>Paragraphe 2</p>
        <div>
            <p>Paragraphe 3 <b>Un joli<b>petit</b><i>contenu</i></b></p>
        </div>
    </div>
</body>
</html>
"""

# Analyser le contenu HTML avec BeautifulSoup
soup = BeautifulSoup(html_content, 'html.parser')

# Fonction récursive pour parcourir les tags
def parcourir_tags(tag, dico):
    print(tag.name)  # Afficher le nom du tag
    if tag.name in dico:
        dico[tag.name] = str(dico[tag.name])+" -- "+str(tag.extract()) #unwrap
    else:
        dico[tag.name] = str(tag.extract()) #unwrap
    #
    for child in tag.children:
        if child.name is not None:
            parcourir_tags(child, dico)

# Appeler la fonction récursive pour commencer le parcours depuis le haut de l'arbre
d = {}
parcourir_tags(soup, d)

print(d)
