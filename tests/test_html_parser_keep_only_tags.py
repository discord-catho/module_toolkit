import sys, os
import unittest
from bs4 import BeautifulSoup

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#
sys.path.insert(0, '../')
from dktotoolkit.parserhtml import _keep_only_selected_tags

class TestKeepOnlySelectedTags(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_keep_only_selected_tags)
        #
        pass
    #endDef

    def test_no_tags_to_keep(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, world!</p>This is important.<br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=[])
        self.assertEqual(cleaned_html, expected_output)

    def test_tags_to_keep(self):
        # Default:
        # - delete_attributes = False
        # - keep_newlines = True
        # - encadre span = False
        # - remove_tag_no_content = False
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'])
        self.assertEqual(cleaned_html, expected_output)

    def test_keep_only_selected_tags_with_empty_content(self):
        html_content = ''
        expected_output = ''
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'])
        self.assertEqual(cleaned_html, expected_output)

    # remove_tag_no_content (default: False)
    def test_keep_only_remove_tag_no_content(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/><p></p>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], remove_tag_no_content=True)
        self.assertEqual(cleaned_html, expected_output)
        
    def test_keep_only_remove_tag_no_contentFalse(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/><p></p>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/><p></p>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], remove_tag_no_content=False)
        self.assertEqual(cleaned_html, expected_output)        

    # keepnewlines (default: True)
    def test_keep_only_selected_tags_keepnewlines(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], keep_newlines=True)
        self.assertEqual(cleaned_html, expected_output)


    def test_keep_only_selected_tags_keepnewlinesFalse(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = 'Hello, <b>world!</b><span class="highlight">This is <i>important</i>.</span>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], keep_newlines=False)
        self.assertEqual(cleaned_html, expected_output)

    def test_keep_only_selected_tags_keepnewlinesFalse_br(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = 'Hello, <b>world!</b><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i', 'br'], keep_newlines=False)
        self.assertEqual(cleaned_html, expected_output)

    def test_keep_only_selected_tags_keepnewlinesFalse_p(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i', 'p'], keep_newlines=False)
        self.assertEqual(cleaned_html, expected_output)
    
    # delete_attributes (default: False)
    def test_tags_to_keep_deleteattributes(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span>This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], delete_attributes=True)
        self.assertEqual(cleaned_html, expected_output)

    def test_tags_to_keep_deleteattributesFalse(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], delete_attributes=False)
        self.assertEqual(cleaned_html, expected_output)
       
    # encadre span (default: False)
    def test_tags_to_keep_encadrespan_div(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i', 'div'], encadre_avec_span=True)
        self.assertEqual(cleaned_html, expected_output)

    def test_tags_to_keep_encadrespan_nodiv(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<span><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</span>'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], encadre_avec_span=True)
        self.assertEqual(cleaned_html, expected_output)

    def test_tags_to_keep_encadrespanFalse_div(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i', 'div'], encadre_avec_span=False)
        self.assertEqual(cleaned_html, expected_output)

    def test_tags_to_keep_encadrespanFalse_nodiv(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], encadre_avec_span=False)
        self.assertEqual(cleaned_html, expected_output)


    # removeInside (default: False)
    def test_tags_to_keep_removeInsideFalse(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], remove_inside=False)
        self.assertEqual(cleaned_html, expected_output)

    def test_tags_to_keep_removeInside_div(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<div><p>Hello, </p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['div', 'span', 'i'], remove_inside=True)
        self.assertEqual(cleaned_html, expected_output)
        
    def test_tags_to_keep_removeInside_div_nonewline(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span><br/>Coucou!</div>'
        expected_output = '<div><span class="highlight">This is <i>important</i>.</span>Coucou!</div>'
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['div', 'span', 'i'], remove_inside=True, keep_newlines=False)
        self.assertEqual(cleaned_html, expected_output)
        
    def test_tags_to_keep_removeInside_b(self):
        html_content = '<div><p>Hello, <b>world!</b></p><span class="highlight">This is <i>important</i>.</span></div>'
        expected_output = ''
        cleaned_html = _keep_only_selected_tags(html_content, tags_to_keep=['b', 'span', 'i'], remove_inside=True)
        self.assertEqual(cleaned_html, expected_output)


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef

if __name__ == '__main__':
    unittest.main()
