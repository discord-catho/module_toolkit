# -*- coding: utf-8 -*-
import os
import unittest  # python -m unittest discover
import sys
from datetime import date, timedelta

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.functions import compatMode


class TestFunctions(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(compatMode)
        #
        pass
    #endDef

    # nom_var = myvariable
    # alternatives = "my_variable"
    # alternatives = ["my_variable",]
    # kwargs : {"myvariable":"valeur", "coucou":"1"}
    #

    def test_0(self):
        result = compatMode("myvariable", list_var=["my_variable", "myvariable", "coucou"], verbose=False)
        self.assertEqual(result, (None, {}))
    #endDef


    def test_1(self):
        result = compatMode("myvariable", list_var=["my_variable", "myvariable", "myvar"], my_variable="valeur", verbose=False)
        self.assertEqual(result, ("valeur",{}))
    #endDef

    def test_5(self):
        result = compatMode("myvariable", list_var=["my_variable", "myvariable", "myvar"], my_variable="valeur", myvar="valeur2", verbose=False)
        self.assertEqual(result, ("valeur2",{}))
    #endDef

    def test_2(self):
        result = compatMode("myvariable", list_var=["my_variable", "myvariable", "myvar"], my_variable="valeur", hello="world", verbose=False)
        self.assertEqual(result, ("valeur",{"hello":"world"}))
    #endDef


    def test_3(self):
        result = compatMode("myvariable", list_var="my_variable", my_variable="valeur", verbose=False)
        self.assertEqual(result, ("valeur",{}))
    #endDef


    def test_4(self):
        result = compatMode("myvariable", list_var=["my_variable",], my_variable="valeur", verbose=True)
        self.assertEqual(result, ("valeur",{}))
    #endDef


    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
#endClass
