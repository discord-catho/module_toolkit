import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestDaysMinutes(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days_frequenceminutes(self):
        b = [
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 0},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 180},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 360},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 540},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 720},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 900},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 1080},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 1260},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 1440},
        ]
        self.assertCountEqual(prepare_schedule_params(days=([5,]), hours=(), minutes=(["/180", ])), b)
    #

    def test_days_frequenceminutes(self):
        b = [
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 10},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR:-1, MINUTE: 20},
        ]
        self.assertCountEqual(prepare_schedule_params(days=([5,]), hours=(), minutes=(["10","20" ])), b)
    #
#

if __name__ == '__main__':
    unittest.main()
