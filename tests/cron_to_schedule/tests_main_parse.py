import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestParse():#unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days(self):
        a = prepare_schedule_params(days=1, hours=(), minutes=())
        b = [
            {PERIOD: DAY[1], INTERVAL: 1, HOUR: None, MINUTE: None},
        ]
        self.assertEqual(a, b)
    #
    def test_hours(self):
        a = prepare_schedule_params(days=None, hours=1)
        b = [
            {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: None},
        ]
        self.assertEqual(a, b)
    #
    def test_minutes(self):
        a = prepare_schedule_params(minutes=1)
        b = [
            {PERIOD: 'minutes', INTERVAL: 1, HOUR: None, MINUTE: None},
        ]
        self.assertEqual(a, b)
    #

if __name__ == '__main__': 
    unittest.main() 
