import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestDaysHours(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days_hours(self):
        b =[
            {PERIOD: DAY[1], INTERVAL: 1, HOUR: 1, MINUTE: None},
            {PERIOD: DAY[1], INTERVAL: 1, HOUR: 5, MINUTE: None},
            {PERIOD: DAY[1], INTERVAL: 1, HOUR: 12, MINUTE: None}
        ]
        self.assertCountEqual(b, prepare_schedule_params(days=(1,), hours=(1,5,12), minutes=()))

    #

    def test_days_interv_from_hours(self):
        b =[
            {PERIOD: DAY[2], INTERVAL: 1, HOUR: 1, MINUTE: None},
            {PERIOD: DAY[2], INTERVAL: 1, HOUR: 5, MINUTE: None},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR: 1, MINUTE: None},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR: 5, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=("2/3",), hours=(1,5,), minutes=()), b)
    #

    def test_days_frequence_hour(self):
        b = [
            {PERIOD: DAY[0], INTERVAL: 3, HOUR: 5, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 3, HOUR: 7, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=("/3",), hours=(5,7), minutes=()), b)
    #

    def test_days_frequence_hour_frequence(self):
        b = [
            {PERIOD: DAY[0], INTERVAL: 3, HOUR: 0, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 3, HOUR: 10, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 3, HOUR: 20, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=("/3",), hours=("/10",), minutes=()), b)
    #

#

if __name__ == '__main__':
    unittest.main()
