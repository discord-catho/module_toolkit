import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestDaysHoursMinutes(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days_minutes(self):
        b = [
            {PERIOD: DAY[5], INTERVAL: 1, HOUR: -1, MINUTE: 10},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR: -1, MINUTE: 30},
        ]
        self.assertCountEqual(prepare_schedule_params(days=([5,]), hours=(), minutes=(["10",30 ])), b)
    #

#

if __name__ == '__main__':
    unittest.main()
