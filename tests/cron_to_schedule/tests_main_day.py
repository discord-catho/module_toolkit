import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestDays(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days_only(self):
        a = prepare_schedule_params(days=("1","2/3",), hours=(), minutes=())
        b = [
            {PERIOD: DAY[1], INTERVAL: 1, HOUR: None, MINUTE: None},
            {PERIOD: DAY[2], INTERVAL: 1, HOUR: None, MINUTE: None},
            {PERIOD: DAY[5], INTERVAL: 1, HOUR: None, MINUTE: None}
        ]
        self.assertCountEqual(a, b)
    #

    def test_days_frequence_only(self):
        a = prepare_schedule_params(days=("/3",), hours=(), minutes=())
        b = [
            {PERIOD: DAY[0], INTERVAL: 3, HOUR: None, MINUTE: None},
        ]
        self.assertCountEqual(a, b)
    #


#

if __name__ == '__main__':
    unittest.main()
