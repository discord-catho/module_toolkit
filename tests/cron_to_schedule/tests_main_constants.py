import unittest

from __constants import *

class TestConstants(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days(self):
        self.assertEqual(DAY[0], "days")
    #

    def test_monday(self):
        self.assertEqual(DAY[1], "monday")
    #

    def test_tuesday(self):
        self.assertEqual(DAY[2], "tuesday")
    #

    def test_wednesday(self):
        self.assertEqual(DAY[3], "wednesday")
    #

    def test_thursday(self):
        self.assertEqual(DAY[4], "thursday")
    #

    def test_friday(self):
        self.assertEqual(DAY[5], "friday")
    #

    def test_saturday(self):
        self.assertEqual(DAY[6], "saturday")
    #


    def test_sunday(self):
        self.assertEqual(DAY[7], "sunday")
    #

    def test_minute(self):
        self.assertEqual(MINUTE, "minutes")
    #

    def test_hour(self):
        self.assertEqual(HOUR, "hours")
    #

    def test_interval(self):
        self.assertEqual(INTERVAL, "interval")
    #

    def test_period(self):
        self.assertEqual(PERIOD, "period")
    #

#

if __name__ == '__main__':
    unittest.main()
