import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestHours(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_hours(self):
        b =[
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 1, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 5, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 12, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=(), hours=(1,5,12), minutes=()), b)
    #


    def test_hours_list(self):
        b =[
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 1, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 5, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 12, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=(), hours=(1,5,12), minutes=()), b)
    #

    def test_hours_interv_from(self):

        b = [
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 2, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 12, MINUTE: None},
            {PERIOD: DAY[0], INTERVAL: 1, HOUR: 22, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=(), hours=("2/10",), minutes=()), b)
    #

    def test_hours_interv(self):
        b =[
            {PERIOD: HOUR, INTERVAL: 10, HOUR: None, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=(), hours=("/10",), minutes=()), b)
    #
#
if __name__ == '__main__':
    unittest.main()
