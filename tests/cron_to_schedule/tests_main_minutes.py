import unittest

from _prepare_schedule_params import prepare_schedule_params
from __constants import *

class TestMinutes(unittest.TestCase):
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.maxDiff = None
    #

    def test_days_only(self):
        b = [
            {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: 1},
            {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: 5},
        ]
        self.assertCountEqual(prepare_schedule_params(days=(), hours=(), minutes=("1","5",)), b)
    #

    def test_days_frequence_only(self):
        b = [
            {PERIOD: MINUTE, INTERVAL: 15, HOUR: None, MINUTE: None},
        ]
        self.assertCountEqual(prepare_schedule_params(days=(), hours=(), minutes=("/15",)), b)
    #

    def test_days_frequence_start_only(self):
         b = [
             {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: 2},
             {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: 17},
             {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: 32},
             {PERIOD: HOUR, INTERVAL: 1, HOUR: None, MINUTE: 47},
         ]
         self.assertCountEqual(prepare_schedule_params(days=(), hours=(), minutes=("2/15",)), b)
     #
#

if __name__ == '__main__':
    unittest.main()
