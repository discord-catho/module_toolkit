# -*- coding: utf-8 -*-
import unittest  # python -m unittest discover
import sys
import os

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.envvar import load_dotenv, getEnvironVar


class TestDotenv(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test

        with open("./test.env", "w") as f:
            f.write("TEST_VAR=1\nTEST2=3\nTEST3=4:5:6.0\nTEST5=\\xa0")
        #endWidth

        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(load_dotenv)
        #
    #endDef

    def test_0(self):
        result = load_dotenv("./test.env", erase_variable=False)
        test_var = os.environ.get("TEST_VAR", None)
        test2 = os.environ.get("TEST2", None)
        test3 = os.environ.get("TEST3", None)
        self.assertEqual(result, None)
        self.assertEqual(test_var, '1')
        self.assertEqual(test2, '3')
        self.assertEqual(test3, '4:5:6.0')
    #endDef

    def test_erase(self):
        os.environ["TEST_VAR"] = "10"
        result = load_dotenv("./test.env", erase_variable=True)
        test_var = os.environ.get("TEST_VAR", None)
        test2 = os.environ.get("TEST2", None)
        test3 = os.environ.get("TEST3", None)
        self.assertEqual(result, None)
        self.assertEqual(test_var, '10')
        self.assertEqual(test2, '3')
        self.assertEqual(test3, '4:5:6.0')
    #endDef

    def test_noterase(self):
        os.environ["TEST_VAR"] = "10"
        result = load_dotenv("./test.env", erase_variable=False)
        test_var = os.environ.get("TEST_VAR", None)
        test2 = os.environ.get("TEST2", None)
        test3 = os.environ.get("TEST3", None)
        self.assertEqual(result, None)
        self.assertEqual(test_var, '1')
        self.assertEqual(test2, '3')
        self.assertEqual(test3, '4:5:6.0')
    #endDef

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        os.remove("./test.env")

    #endDef
#endClass


class Test_getEnvironVar(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test

        with open("./test.env", "w") as f:
            f.write("TEST_VAR=1\nTEST2=3\nTEST3=4:5:6.0\nTEST5=\\xa0")
        #endWidth

        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(getEnvironVar)
        #
    #endDef


    def test_GetEnvironVar_str(self):
        os.environ["coucou"] = "3"
        result = getEnvironVar("coucou")
        self.assertEqual(result, "3")
    #endDEf

    def test_GetEnvironVar_liststr(self):
        os.environ["coucou"] = "3:4:5"
        result = getEnvironVar("coucou")
        self.assertEqual(result, ["3","4","5"])
    #endDEf

    def test_GetEnvironVar_int(self):
        os.environ["coucou"] = "3"
        result = getEnvironVar("coucou", digit=True)
        self.assertEqual(result, 3)
    #endDEf

    def test_GetEnvironVar_float(self):
        os.environ["coucou"] = "3.5"
        result = getEnvironVar("coucou", digit=True)
        self.assertEqual(result, 3.5)
    #endDEf

    def test_GetEnvironVar_digitstr(self):
        os.environ["coucou"] = "a"
        result = getEnvironVar("coucou", digit=True)
        self.assertEqual(result, "a")
    #endDEf

    def test_GetEnvironVar_listint(self):
        os.environ["coucou"] = "3:4.5:5"
        result = getEnvironVar("coucou", digit=True)
        self.assertEqual(result, [3,4.5,5])
    #endDEf

    def test_GetEnvironVar_None(self):
        result = getEnvironVar(None)
        self.assertEqual(result, None)
    #endDEf

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        os.remove("./test.env")

    #endDef
#endClass
