import sys, os
import unittest
from bs4 import BeautifulSoup
import html

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#
sys.path.insert(0, '../')

from dktotoolkit.parserhtml import _convert_html_to_markdown

class TestHTML2MD(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_convert_html_to_markdown)
        #
        pass
    #endDef


    def test_convert_html_to_markdown_simple(self):
        html_content = '<b>Bold text</b>'
        expected_output = '**Bold text**'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_link(self):
        html_content = '<a href="https://example.com">Link text</a>'
        expected_output = '[Link text](https://example.com)'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_heading1(self):
        html_content = '<h1>Heading 1</h1>'
        expected_output = '# Heading 1'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_heading3(self):
        html_content = '<h3>Heading 3</h3>'
        expected_output = '### Heading 3'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_heading1_withtxt(self):
        html_content = 'Coucou<h1>Heading 1</h1>Un texte'
        expected_output = '''Coucou

# Heading 1

Un texte'''
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_heading3_withtxt(self):
        html_content = 'Coucou<h3>Heading 3</h3>Un texte'
        expected_output = '''Coucou
### Heading 3

Un texte'''
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)
    def test_convert_html_to_markdown_with_unordered_list(self):
        html_content = '<ul><li>Item 1</li><li>Item 2</li></ul>'
        expected_output = """- Item 1
- Item 2"""
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_line_break(self):
        html_content = 'Line 1<br>Line 2'
        expected_output = '''Line 1
Line 2'''
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    # def test_convert_html_to_markdown_with_line_breakFalse(self):
    #     html_content = 'Line 1<br>Line 2'
    #     expected_output = '''Line 1 Line 2'''
    #     markdown = _convert_html_to_markdown(html_content, preserve_linebreaks=False)
    #     self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_blockquote(self):
        html_content = '<blockquote>Quoted text</blockquote>'
        expected_output = '>>> Quoted text'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_quote(self):
        html_content = '<quote>Quoted text</quote>'
        expected_output = '> Quoted text'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_with_code_block(self):
        html_content = '<pre><code>print("Hello, world!")</code></pre>'
        expected_output = '''```
print("Hello, world!")
```'''
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_nested(self):
        html_content = '<b>Bold and <i>italic</i></b>'
        expected_output = '**Bold and *italic***'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def test_convert_html_to_markdown_table(self):
        html_content = '<table><tr><th>Header 1</th><th>Header 2</th></tr><tr><td>Data 1</td><td>Data 2</td></tr></table>'
        expected_output = '| Header 1 | Header 2 |\n| --- | --- |\n| Data 1 | Data 2 |'
        markdown = _convert_html_to_markdown(html_content)
        self.assertEqual(markdown, expected_output)

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
#

if __name__ == '__main__':
    unittest.main()
