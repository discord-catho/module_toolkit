# -*- coding: utf-8 -*-
import os
import unittest  # python -m unittest discover
import sys

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#

sys.path.insert(0, '../')
from dktotoolkit.dict import dict2obj, invert_dict


class Expected:
    def __init__(self):
        self.bonjour = "Hello world!"
        self.bye = "Bye world!"
        pass
    #endDef
#endClass

class Empty():
    def __init__(self):
        pass
    #endDef
#endClass

class TestDict(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(dict2obj)
        #
        pass
    #endDef

    def test_0(self):
        dico_in = {"bonjour":"Hello world!", "bye":"Bye world!"}
        result = dict2obj(dico_in).__dict__
        e = Expected().__dict__
        self.assertEqual(result, e)
    #endDef

    def test_1(self):
        dico_in = {"bonjour":"Hello world!", "bye":"Bye world!"}
        list_in = [dico_in, dico_in]
        result = dict2obj(list_in)
        e = Expected().__dict__
        self.assertEqual([a.__dict__ for a in result], [e, e])
    #endDef

    def test_2(self):
        dico_in = {"bonjour":"Hello world!", "bye":"Bye world!"}
        result = dict2obj(dico_in, Empty()).__dict__
        e = Expected().__dict__
        self.assertEqual(result, e)
    #endDef

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef

    class TestInvertDict(unittest.TestCase):
        def setUp(self):
            # Code execute avant chaque test
            if os.environ.get("PROFILING", False):
                self.profiler, self.delete_profiling_env = hr.setUp_profiler(dict2obj)
            #
            pass
        #endDef

        def test_invert_dict(self):
            dictionary = {'a': 1, 'b': 2, 'c': 3}
            inverted_dict = invert_dict(dictionary)
            expected_dict = {1: 'a', 2: 'b', 3: 'c'}
            self.assertEqual(inverted_dict, expected_dict)
        #

        def tearDown(self):
            # Code executé après chaque test
            #del self.my_class_instance
            if os.environ.get("PROFILING", False):
                output_folder="./profile"

                proffile_basename = hr.names2basename(
                    file_name = __file__,
                    class_name = self.__class__.__name__,
                    method_name = self._testMethodName
                )

                hr.tearDown_profiler(
                    profiler=self.profiler,
                    proffile_basename=proffile_basename,
                    output_folder=output_folder,
                    delete_profiling_env=self.delete_profiling_env
                )
            #
        #

        pass
    #endDef
#endClass
