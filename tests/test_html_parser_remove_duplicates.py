import sys, os
import unittest
from bs4 import BeautifulSoup
import html

if os.environ.get("PROFILING", False):
    # Le coverage classique doit etre lance apres le profiling
    # Et en supprimant les donnees coverage du profiling
    # Sinon, le coverage est fausse
    import html_report_line_profiler as hr
#
sys.path.insert(0, '../')
from dktotoolkit.parserhtml import _remove_duplicate_tags


class TestRemoveDuplicateTags(unittest.TestCase):
    def setUp(self):
        # Code execute avant chaque test
        if os.environ.get("PROFILING", False):
            self.profiler, self.delete_profiling_env = hr.setUp_profiler(_remove_duplicate_tags)
        #
        pass
    #endDef

    def test_remove_duplicate_tags(self):
        html_content = '<b><b>Hello, <i><i>world!</i></i></b></b>'
        expected_output = '<b>Hello, <i>world!</i></b>'
        cleaned_html = _remove_duplicate_tags(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_remove_duplicate_tags_nested(self):
        html_content = '<b><i><b><i>Hello, <b>world!</b></i></b></i></b>'
        expected_output = '<b><i>Hello, world!</i></b>'
        cleaned_html = _remove_duplicate_tags(html_content)
        self.assertEqual(cleaned_html, expected_output)
        
    def test_remove_duplicate_tags_complique(self):
        html_content = '<b>Bonjour <i>tout <b>le monde !</b></i> <b>Ceci</b> est <i><i>un</i></i> un </b> test.'
        expected_output = '<b>Bonjour <i>tout le monde !</i> Ceci est <i>un</i> un </b> test.'
        cleaned_html = _remove_duplicate_tags(html_content)
        self.assertEqual(cleaned_html, expected_output)
        
    def test_remove_duplicate_tags_no_duplicates(self):
        html_content = '<b>Hello, <i>world!</i></b>'
        expected_output = '<b>Hello, <i>world!</i></b>'
        cleaned_html = _remove_duplicate_tags(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_remove_duplicate_tags_no_tags(self):
        html_content = 'Hello, world!'
        expected_output = 'Hello, world!'
        cleaned_html = _remove_duplicate_tags(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def test_remove_duplicate_tags_empty(self):
        html_content = ''
        expected_output = ''
        cleaned_html = _remove_duplicate_tags(html_content)
        self.assertEqual(cleaned_html, expected_output)

    def tearDown(self):
        # Code executé après chaque test
        #del self.my_class_instance
        if os.environ.get("PROFILING", False):
            output_folder="./profile"

            proffile_basename = hr.names2basename(
                file_name = __file__,
                class_name = self.__class__.__name__,
                method_name = self._testMethodName
            )

            hr.tearDown_profiler(
                profiler=self.profiler,
                proffile_basename=proffile_basename,
                output_folder=output_folder,
                delete_profiling_env=self.delete_profiling_env
            )
        #

        pass
    #endDef
#

if __name__ == '__main__':
    unittest.main()
